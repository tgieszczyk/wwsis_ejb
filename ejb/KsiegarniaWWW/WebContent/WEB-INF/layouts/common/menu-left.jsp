<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!-- LISTWA MENU -->

<div class="template_left_menu_bar">
	<p class="template_left_menu_bar_title">Przyklady CRUD</p>

	<ul class="clear">
		<c:url var="menuUrl1" value="/crud/autor/lista.htm"></c:url>
		<li><a href="${menuUrl1}">Autorzy</a></li>

		<c:url var="menuUrl2" value="/crud/ksiazka/lista.htm"></c:url>
		<li><a href="${menuUrl2}">Ksiazki</a></li>

		<c:url var="menuUrl3" value="/crud/autor/nowy-autor.jsp"></c:url>
		<li><a href="${menuUrl3}">Dodaj nowego Autora</a></li>

	</ul>
	<div class="clear"></div>
</div>