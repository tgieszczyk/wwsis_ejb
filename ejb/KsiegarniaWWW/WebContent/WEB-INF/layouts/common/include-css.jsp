<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/styles_main.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/styles_form.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/styles_table.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/jqueryui/jquery-ui.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/jqueryui/jquery-ui.structure.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/jqueryui/jquery-ui.theme.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/hint.css">
<link type="text/css" rel=stylesheet href="<%=request.getContextPath()%>/css/jquery.timepicker.css">
