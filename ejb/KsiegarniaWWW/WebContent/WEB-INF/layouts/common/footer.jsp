<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>
<!-- STOPKA -->
	
<div class="template_footer clear">
	<div class="template_footer_left">
		<a href="http://www.tmg-software.pl">www.tmg-software.pl</a> 
	</div>
	<div class="template_footer_right">Copyright &copy; 2008-2016 TMG-Software.pl | Wszelkie prawa zastrzeżone!</div>
</div>