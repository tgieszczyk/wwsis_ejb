<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!-- SEKCJA LOGOWANIA -->

<div class="template_header_login_box">
		<jsp:include page="../templates/logon.jsp" />
</div>
<!-- LISTWA TYTUŁOWA -->
<div class="template_title_bar">
	<c:url var="easycallLogoUrl" value="/images/easycall_logo.jpg" />
	<c:url var="tmgLogoUrl" value="/images/logo-tmg.png" />
	<img class="easycall_logo" src="${easycallLogoUrl}" height="30" alt="EasyCall" /> 
	<img class="client_logo" src="${tmgLogoUrl}" height="30" alt="TMG Software" />
</div>