<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!-- INFORMACJA O ZALOGOWANYM UŻYTKOWNIKU -->
<form class="template_header_login_box_logged_form"
	action="<c:url value="/logout.htm" />">
	<p class="template_header_login_box_logged_info">Witaj na wykładzie - Studencie</p>
</form>