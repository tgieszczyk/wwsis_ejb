<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
<jsp:include page="/WEB-INF/layouts/common/include-head.jsp" />
<jsp:include page="index-js.jsp" />
</head>
<body>

	<script>
		function navigateTo(url) {
			window.location = url;
		}

		function submitForm(formId, url) {
			jQuery('#' + formId).attr('action', url);
			return true;
		}

		function doSubmitForm(formId, url) {
			var valid = validateForm(formId);
			if (valid) {
				submitForm(formId, url);
				jQuery("#" + formId).submit();
			}
			return false;
		}

		function doSubmitFormWithNoValidation(formId, url) {
			jQuery("#" + formId).validate().settings.ignore = "*";
			submitForm(formId, url);
			jQuery("#" + formId).submit();
			return false;
		}
	</script>
	<a id="backUrl" style="display: none"
		href="<%=request.getContextPath()%>/back">back</a>

	<div class="template_all">

		<!-- NAGŁÓWEK -->
		<div class="template_header">
			<jsp:include page="/WEB-INF/layouts/common/header.jsp" />

			<jsp:include page="/WEB-INF/layouts/common/menu-top.jsp" />
		</div>

		<!-- LEWE MENU -->

		<div class="template_left">
			<jsp:include page="/WEB-INF/layouts/common/menu-left.jsp" />
		</div>

		<!-- CENTRALNA SEKCJA STRONY -->

		<div class="template_center">

			<c:if test="${not empty param.token_error}">
				<p class="actionError">Błąd</p>
			</c:if>

			<!-- tutaj CONTENT strony -->
<!-- koniec kontentu -->
			<div id="ajaxPopupDiv" class="popupDiv"
				style="margin: 5px; padding: 5px; display: none"></div>

		</div>

		<!-- STOPKA -->
		<div class="template_footer clear">
			<%@ include file="/WEB-INF/layouts/common/footer.jsp"%>
		</div>
	</div>
</body>
</html>