<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<script>
	$(function() {
		$("#dataWydania").datepicker({
			dateFormat: "yy-mm-dd"});
	});
	
	function navigateTo(url) {
		window.location = url;
	}

	function submitForm(formId, url) {
		jQuery('#' + formId).attr('action', url);
		return true;
	}

	function doSubmitForm(formId, url) {
		var valid = validateForm(formId);
		if (valid) {
			submitForm(formId, url);
			jQuery("#" + formId).submit();
		}
		return false;
	}

	function doSubmitFormWithNoValidation(formId, url) {
		jQuery("#" + formId).validate().settings.ignore = "*";
		submitForm(formId, url);
		jQuery("#" + formId).submit();
		return false;
	}
</script>