<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
<jsp:include page="/WEB-INF/layouts/common/include-head.jsp" />
<jsp:include page="nowa-ksiazka-js.jsp" />
</head>
<body>
	<a id="backUrl" style="display: none"
		href="<%=request.getContextPath()%>/back">back</a>

	<div class="template_all">

		<!-- NAGŁÓWEK -->
		<div class="template_header">
			<jsp:include page="/WEB-INF/layouts/common/header.jsp" />

			<jsp:include page="/WEB-INF/layouts/common/menu-top.jsp" />
		</div>

		<!-- LEWE MENU -->

		<div class="template_left">
			<jsp:include page="/WEB-INF/layouts/common/menu-left.jsp" />
		</div>

		<!-- CENTRALNA SEKCJA STRONY -->

		<div class="template_center">

			<c:if test="${not empty param.token_error}">
				<p class="actionError">Błąd</p>
			</c:if>


			<div class="template_form">
				<h1>Dodawanie nowej Ksiazki do bazy</h1>
				<br /> <br />
				<c:url value="/crud/ksiazka/zapisz.htm" var="saveUrl"></c:url>
				<form id="ksiazkaFormId" action="${saveUrl}" method="post">
					<br />
					<fieldset>
						<legend>Dane </legend>

						<div style="width: 100%">
							<table style="width: 100%">
							
<!-- 							<td>Tytul</td> -->
<!-- 								<td>Nazwa wydawsnictwa</td> -->
<!-- 								<td>Data wydania</td> -->
<!-- 								<td>Kategoria</td> -->
<!-- 								<td>Opis</td> -->
<!-- 								<td>Cena</td> -->
								
								
								<tr>
									<td align="right" width="20%">Tytul:</td>
									<td align="left"><input name="tytul" class="required" /></td>
								</tr>
								<tr>
									<td align="right" width="20%">Data Wydania:</td>
									<td align="left"><input id="dataWydania" name="dataWydania" class="required" /></td>
								</tr>
								<tr>
									<td align="right" width="20%">Wydawnictwo:</td>
									<td align="left"><select name="wydawnictwoId" class="required" >
											<option value="">Brak</option>
											<c:forEach items="${ listaWydawnictw }" var="wydawnictwo">
												<option value="${wydawnictwo.id }">${wydawnictwo.nazwa }</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td align="right" width="20%">Autorzy:</td>
									<td align="left"><select name="autorIdLista" multiple="multiple" class="required">
											<c:forEach items="${ listaAutorow }" var="autor">
												<option value="${autor.id }">${autor.imie} ${autor.nazwisko}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td align="right" width="20%">Opis:</td>
									<td align="left"><textarea rows="4" cols="25" name="opis"
											class="required"></textarea></td>
								</tr>
								<tr>
									<td align="right" width="20%">Kategoria:</td>
									<td align="left"><select name="kategoria" class="required" >
											<option value="">Brak</option>
											<c:forEach items="${ listaKategorii }" var="kategoria">
												<option value="${kategoria}">${kategoria.label}</option>
											</c:forEach>
									</select></td>
								</tr>
								<tr>
									<td align="right" width="20%">Cena:</td>
									<td align="left"><input name="cena" class="required" /></td>
								</tr>
							</table>
						</div>
					</fieldset>

					<br /> <br /> <input type="button" class="button" value="Anuluj" />
					<input type="submit" class="button" value="Dodaj ->"
						onclick="return validateForm('ksiazkaFormId');" />
				</form>

			</div>

			<!--  koniec centralnej sekcji -->

			<div id="ajaxPopupDiv" class="popupDiv"
				style="margin: 5px; padding: 5px; display: none"></div>

		</div>

		<!-- STOPKA -->
		<div class="template_footer clear">
			<%@ include file="/WEB-INF/layouts/common/footer.jsp"%>
		</div>
	</div>
</body>
</html>