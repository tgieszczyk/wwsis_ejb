<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
<jsp:include page="/WEB-INF/layouts/common/include-head.jsp" />
<jsp:include page="lista-autorow-js.jsp" />
</head>
<body>
	<a id="backUrl" style="display: none"
		href="<%=request.getContextPath()%>/back">back</a>

	<div class="template_all">

		<!-- NAGŁÓWEK -->
		<div class="template_header">
			<jsp:include page="/WEB-INF/layouts/common/header.jsp" />

			<jsp:include page="/WEB-INF/layouts/common/menu-top.jsp" />
		</div>

		<!-- LEWE MENU -->

		<div class="template_left">
			<jsp:include page="/WEB-INF/layouts/common/menu-left.jsp" />
		</div>

		<!-- CENTRALNA SEKCJA STRONY -->

		<div class="template_center">

			<c:if test="${not empty param.token_error}">
				<p class="actionError">Błąd</p>
			</c:if>


			<div class="template_form">
				<br />

				<c:url var="nowyAutorUrl" value="/crud/autor/nowy-autor.jsp" />
				<a href="${nowyAutorUrl}"> Dodaj Autora (link) </a>
				<input type="button" value="Dodaj Autora" onclick="navigateTo('${nowyAutorUrl}');" /> <br /> <br />
				<br />
				<fieldset style="width: 80%">
					<legend>Wszyscy autorzy </legend>
					<div style="width: 100%">
						<table style="width: 100%">
							<thead>
								<tr>
									<td>Imie</td>
									<td>nazwisko</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${ listaAutorow }" var="autor">
									<tr>
										<td width="50%">${autor.imie }</td>
										<td width="50%">${autor.nazwisko }</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>

				<br /> <br />

			</div>

			<!--  koniec centralnej sekcji -->

			<div id="ajaxPopupDiv" class="popupDiv"
				style="margin: 5px; padding: 5px; display: none"></div>

		</div>

		<!-- STOPKA -->
		<div class="template_footer clear">
			<%@ include file="/WEB-INF/layouts/common/footer.jsp"%>
		</div>
	</div>
</body>
</html>