<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/layouts/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
<jsp:include page="/WEB-INF/layouts/common/include-head.jsp" />
<jsp:include page="nowy-autor-js.jsp" />
</head>
<body>
	<a id="backUrl" style="display: none"
		href="<%=request.getContextPath()%>/back">back</a>

	<div class="template_all">

		<!-- NAGŁÓWEK -->
		<div class="template_header">
			<jsp:include page="/WEB-INF/layouts/common/header.jsp" />

			<jsp:include page="/WEB-INF/layouts/common/menu-top.jsp" />
		</div>

		<!-- LEWE MENU -->

		<div class="template_left">
			<jsp:include page="/WEB-INF/layouts/common/menu-left.jsp" />
		</div>

		<!-- CENTRALNA SEKCJA STRONY -->

		<div class="template_center">

			<c:if test="${not empty param.token_error}">
				<p class="actionError">Błąd</p>
			</c:if>


			<div class="template_form">
				<h1>
					Dodawanie nowego Autora do bazy
				</h1>
				<br /> <br />
				<c:url value="/crud/autor/zapisz.htm" var="saveUrl"></c:url>
				<form id="ksiazkaFormId" action="${saveUrl}" method="post">
					<br />
					<fieldset>
						<legend>Dane
						</legend>

						<div style="width: 100%">
							<table style="width: 100%">
								<tr>
									<td align="right" width="20%">Imie:</td>
									<td align="left"><input name="imie" class="required"/></td>
								</tr>
								<tr>
									<td align="right" width="20%">Nazwisko:</td>
									<td align="left"><input name="nazwisko" class="required"/></td>
								</tr>
							</table>
						</div>
					</fieldset>

					<br />
					<br />
					<input type="button" class="button" value="Anuluj"/>
					<input type="submit" class="button" value="Dodaj ->"
						onclick="return validateForm('ksiazkaFormId');" />
				</form>

			</div>

<!--  koniec centralnej sekcji -->

			<div id="ajaxPopupDiv" class="popupDiv"
				style="margin: 5px; padding: 5px; display: none"></div>

		</div>

		<!-- STOPKA -->
		<div class="template_footer clear">
			<%@ include file="/WEB-INF/layouts/common/footer.jsp"%>
		</div>
	</div>
</body>
</html>