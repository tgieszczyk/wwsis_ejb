function clearForm(formId) {
	var coll = document.getElementById(formId)['elements'];
	for (i = 0; i < coll.length; i++) {
		if ((coll[i].type.indexOf('text') != -1)
				|| (coll[i].type.indexOf('select') != -1)) {
			coll[i].value = '';
		}
	}
}

function ajaxRequest(divId, url) {
	$('#' + divId).html('');
	$.get(url, function(response) {
		$('#' + divId).fadeOut(
				'fast',
				function() {
					$('#' + divId).html(
							'<!--[if lte IE 6.5]><iframe id="hiddenIframe"></iframe><![endif]-->'
									+ response);
					$('#' + divId).fadeIn('fast');
				});
	});
}

function ajaxGetHTML(url) {
	$.get(url, function(response) {
		return response;
	});
}

function ajaxShowImage(url, obj, e) {
	var code = '<img src="' + url + '" border="0"/>';
	showhint(code, obj, e, '');
}

function validateForm(formId) {
	return $("#" + formId).valid();
}

function validatePopupForm(formId) {
	return $("#" + formId).valid();
}

function checkAllBoxes(multiboxName, checked) {
	var multibox = document.getElementsByName(multiboxName);
	for (i = 0; i < multibox.length; i++) {
		if (!multibox[i].disabled)
			multibox[i].checked = checked;
	}
}

function toogleDiv(divId, left, top, fullScreenId, otherElementIds) {
	var div = document.getElementById(divId);
	var arrPageSizes = ___getPageSize();
	var arrPageScroll = ___getPageScroll();
	div.style.position = "absolute";
	if (otherElementIds != null) {
		for (var i = 0; i < otherElementIds.length; i++) {
			var element = document.getElementById(otherElementIds[i]);
			element.style.visibility = (element.style.visibility == "visible"
					|| element.style.visibility == "" ? "hidden" : "visible");
		}
	}
	div.style.visibility = (div.style.visibility == "visible" ? "hidden"
			: "visible");
	div.style.display = (div.style.display == "block" ? "none" : "block");
	div.style.zIndex = 10;
	var wide = window.screen.width;
	var high = window.screen.heigth;

	div.style.left = arrPageScroll[0] + ((wide / 2) - (div.clientWidth / 2))
			+ "px";
	div.style.top = arrPageScroll[1] + (top / 2) + "px";
	// var searchSelect = document.getElementById('search_execute_entityType');
	// searchSelect.style.visibility =(searchSelect.style.visibility =="visible"
	// ? "hidden" : "visible");

	var fullScreenDiv = document.getElementById(fullScreenId);
	fullScreenDiv.style.width = arrPageSizes[0] + "px";
	fullScreenDiv.style.height = arrPageSizes[1] + "px";
	fullScreenDiv.style.visibility = (fullScreenDiv.style.visibility == "visible" ? "hidden"
			: "visible");
	fullScreenDiv.style.display = (fullScreenDiv.style.display == "block" ? "none"
			: "block");
	fullScreenDiv.style.zIndex = 9;
}

/**
 * / THIRD FUNCTION getPageSize() by quirksmode.com
 * 
 * @return Array Return an array with page width, height and window = width,
 *         height
 */
function ___getPageSize() {
	var xScroll, yScroll;
	if (window.innerHeight && window.scrollMaxY) {
		xScroll = window.innerWidth + window.scrollMaxX;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight) { // =all
		// but
		// Explorer
		// Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, =Mozilla
		// and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	var windowWidth, windowHeight;
	if (self.innerHeight) { // all except Explorer
		if (document.documentElement.clientWidth) {
			windowWidth = document.documentElement.clientWidth;
		} else {
			windowWidth = self.innerWidth;
		}
		windowHeight = self.innerHeight;
	} else if (document.documentElement
			&& document.documentElement.clientHeight) { // Explorer 6 Strict
		// Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}
	// for small pages with total height less then height of the viewport
	if (yScroll < windowHeight) {
		pageHeight = windowHeight;
	} else {
		pageHeight = yScroll;
	}
	// for small pages with total width less then width of the viewport
	if (xScroll < windowWidth) {
		pageWidth = xScroll;
	} else {
		pageWidth = windowWidth;
	}
	arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight);
	return arrayPageSize;
};
/**
 * / THIRD FUNCTION getPageScroll() by quirksmode.com
 * 
 * @return Array Return an array with x,y page scroll values.
 */
function ___getPageScroll() {
	var xScroll, yScroll;
	if (self.pageYOffset) {
		yScroll = self.pageYOffset;
		xScroll = self.pageXOffset;
	} else if (document.documentElement && document.documentElement.scrollTop) { // Explorer
		// 6
		// Strict
		yScroll = document.documentElement.scrollTop;
		xScroll = document.documentElement.scrollLeft;
	} else if (document.body) {// all other Explorers
		yScroll = document.body.scrollTop;
		xScroll = document.body.scrollLeft;
	}
	arrayPageScroll = new Array(xScroll, yScroll);
	return arrayPageScroll;
};

var layoutWidth;

function countPopupLeft(width) {
	return (___getPageSize()[0] - parseInt(width)) / 2;
}

function countPopupTop(height) {
	return (___getPageSize()[1] - parseInt(height)) / 2;
}

function showHideSelects(val) {
	if (val == 0)
		$("select[class!='popupSelect']").css("visibility", "hidden");
	else
		$("select[class!='popupSelect']").css("visibility", "visible");
}

function onlyNumber(id) {
	var zm = document.getElementById(id);
	var l = zm.value;
	l = l.replace('.', ',');
	if (l.substring(l.length - 1) == ',')
		return;
	if (!numberGtZero(l)) {
		zm.value = l.substring(0, l.length - 1);
	}
}

function showPopupForm(url, header, width1, height1) {
	$("#ajaxPopupDiv").html('');
	var tmstmp = new Date().getTime();
	if (url.indexOf('?') == -1)
		url += "?";
	else
		url += "&";
	ajaxRequest("ajaxPopupDiv", url + 'timestamp=' + tmstmp);
	document.getElementById('ajaxPopupDiv').title = header;
	jQuery("#ajaxPopupDiv").dialog({
		width : width1,
		height : height1,
		modal : true
	});
	jQuery("#ajaxPopupDiv").dialog('open');
}

function showPopupForm2(url, header, width1) {
	$("#ajaxPopupDiv").html('');
	var tmstmp = new Date().getTime();
	if (url.indexOf('?') == -1)
		url += "?";
	else
		url += "&";
	ajaxRequest("ajaxPopupDiv", url + 'timestamp=' + tmstmp);
	// document.getElementById('ajaxPopupDiv').title = header;
	jQuery("#ajaxPopupDiv").dialog({
		width : width1,
		modal : true,
		position : { my: "center", at: "top", of: window }
	});
	jQuery("#ajaxPopupDiv").dialog('open');
	return false;
}

function printJsonResponse(messageDivId, json) {
	var messageDiv = $("#" + messageDivId);
	messageDiv.addClass('hide', 500, function(){
		$(this).html('');	
		if (json.messages.length > 0) {
			for (var i = 0; i < json.messages.length; i++) {
				var message = json.messages[i];
				var text = message.text;
				var type = message.type;
				var div = $("<div/>", {
					'class' : 'SPRING-' + type,
					text : text
				});
				$(this).append(div);
			}

			$(this).removeClass('hide', 500);
			
			setTimeout(function(){
				$("#" + messageDivId).addClass('hide', 500);
			}, 15000);
		}
	});
}


function showPopupImage(url, header) {
	var tmstmp = new Date().getTime();
	if (url.indexOf('?') == -1)
		url += "?";
	else
		url += "&";
	ajaxRequest("ajaxPopupDiv", url + 'timestamp=' + tmstmp);
	jQuery("#ajaxPopupDiv").dialog({
		modal : false
	});
	jQuery("#ajaxPopupDiv").dialog('open');
}

function enterclicked(e) {
	var keycode;
	if (window.event)
		keycode = window.event.keyCode;
	else if (e)
		keycode = e.which;
	else
		return false;

	if (keycode == 13) {
		return true;
	} else
		return false;
}

function blur_date_validate(field) {
	if (jQuery('#' + field).val().length < 8)
		return;
	date_format(field);
	try {
		jQuery('#' + field).valid();
	} catch (e) {

	}
}

function sleep(numberMillis) {
	var now = new Date();
	var exitTime = now.getTime() + numberMillis;
	while (true) {
		now = new Date();
		if (now.getTime() > exitTime)
			return;
	}
}

function confirmDialog(message, yes, cancel, successFunction) {
	try {
		jQuery("#ajaxPopupDiv").html(message);
		jQuery("#ajaxPopupDiv").dialog({
			width : 140,
			modal : true,
			buttons: {
				yes : successFunction,
				cancel : function() {
					$("#ajaxPopupDiv").dialog('close');
				}
			}
//			buttons : [ {
//				text : '<spring:message code="label.cancel"/>',
//				click : function() {
//					$("#ajaxPopupDiv").dialog('close');
//				}
//			}, {
//				text : '<spring:message code="label.yes"/>',
//				click : successFunction
//			} ]
		});
		jQuery("#ajaxPopupDiv").dialog('open');
		
	} catch (err) {
		alert(err.message);
	}
}