<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="autorzy-app">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="bootstrap.min.css">
<script type="text/javascript" src="angular.js"></script>
<script type="text/javascript" src="app.js"></script>
<script type="text/javascript" src="jquery.js"></script>

<title>GeneralTab</title>
</head>
<body>
	<div ng-controller="FetchAutorzyController">
		<div ng-repeat="autor in data.model.autorzy">
			<div>{{autor.id}}&nbsp;&nbsp;-&nbsp;&nbsp;{{autor.imie}}&nbsp;&nbsp;{{autor.nazwisko}}</div>
		</div><br/>
		<button ng-click="data.doGet();">pobierz model</button>
		<br /> <br />
		<p>{{data.message}}</p>
	</div>
</body>
</html>