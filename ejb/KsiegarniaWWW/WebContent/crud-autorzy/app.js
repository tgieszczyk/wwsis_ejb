(function() {
	var app = angular.module('autorzy-app', []);

	app.controller('FetchAutorzyController', function($scope, $http) {
		$scope.data = {};
		$scope.data.fieldId = 1077005;
		$scope.data.message = ""; 
		
		$scope.data.doGet = function(item, event) {
			$scope.data.message = "Pobieram Autorów";
			var url = '/ksiegarnia/resources-rs/autorzy/fetch.ajax';

			var tabsResponse = $http.get(url);
			tabsResponse.success(function(data, status, headers, config) {
				$scope.data.message = "Pobieranie autorów sie powiodlo - Success";
				$scope.data.model = data;
			});
		}

		$scope.data.doPost = function(item, event) {
			$scope.data.message = "Invoking Post";
			var url = '/ksiegarnia/resources-rs/autorzy/save.ajax';

			var tabsRequest = $http.post(url, $scope.data.model);
			tabsRequest.success(function(data, status, headers, config) {
				$scope.data.model = data;
				$scope.data.message = "doPost - success";
			});

			tabsRequest.error(function(data, status, headers, config) {
				$scope.data.message = "doPost - error during sending [" + status + "]";
			});
		}

	});
})();