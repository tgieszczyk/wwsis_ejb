package pl.wwsis.www.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.encje.Autor;
import pl.wwsis.services.ejb.KsiegarniaService;

/**
 * Servlet implementation class AutorzyServlet
 */
@WebServlet("/autorzy")
public class AutorzyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@EJB
	private KsiegarniaService ksiegarniaService;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Autor> listaAutorow = ksiegarniaService.znajdzWszystkich();
		
		PrintWriter pw = response.getWriter();
		pw.print("<table>");
		pw.append("<tr>");
		pw.append("<th>Imie</th>");
		pw.append("<th>Nazwisko</th>");
		pw.append("</tr>");
		for(Autor a : listaAutorow) {
			pw.append("<tr>");
			pw.append("<td>").append(a.getImie()).append("</td>");
			pw.append("<td>").append(a.getNazwisko()).append("</td>");
			pw.append("</tr>");
		}

		pw.print("</table>");
		pw.flush();
	}

}
