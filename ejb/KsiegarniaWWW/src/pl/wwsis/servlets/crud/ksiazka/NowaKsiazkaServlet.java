package pl.wwsis.servlets.crud.ksiazka;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.w3.jaxws.services.AutorService;
import pl.wwsis.ejb.w3.jaxws.services.KsiazkaService;
import pl.wwsis.ejb.w3.jaxws.services.WydawnictwoService;
import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.model.KsiazkaInfo;
import pl.wwsis.encje.model.WydawnictwoInfo;
import pl.wwsis.encje.typy.KategoriaKsiazki;

/**
 */
@WebServlet("/crud/ksiazka/nowa.htm")
public class NowaKsiazkaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private WydawnictwoService wydawnictwoService;
	@EJB
	private AutorService autorService;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<WydawnictwoInfo> listaWydawnictw = wydawnictwoService.znajdzWszystkieWydawnictwoInfo();
		List<KategoriaKsiazki> listaKategorii = Arrays.asList(KategoriaKsiazki.values());
		List<Autor> listaAutorow = autorService.znajdzWszystkich();

		request.getSession().setAttribute("listaWydawnictw", listaWydawnictw);
		request.getSession().setAttribute("listaKategorii", listaKategorii);
		request.getSession().setAttribute("listaAutorow", listaAutorow);
		
		response.sendRedirect("/ksiegarnia/crud/ksiazka/nowa-ksiazka.jsp");
	}

}
