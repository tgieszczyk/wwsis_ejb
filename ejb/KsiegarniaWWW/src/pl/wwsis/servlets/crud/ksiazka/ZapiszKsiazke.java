package pl.wwsis.servlets.crud.ksiazka;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.w3.jaxws.services.AutorService;
import pl.wwsis.ejb.w3.jaxws.services.KsiazkaService;
import pl.wwsis.ejb.w3.jaxws.services.WydawnictwoService;
import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.typy.KategoriaKsiazki;


/**
 * Servlet implementation class ZapiszKsiazke
 */
@WebServlet("/crud/ksiazka/zapisz.htm")
public class ZapiszKsiazke extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
    @EJB
    private KsiazkaService ksiazkaService;
    @EJB
    private WydawnictwoService wydawnictwoService;
    @EJB
    private AutorService autorService;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tytul = request.getParameter("tytul");
		String opis = request.getParameter("opis");
		String kategoria = request.getParameter("kategoria");
		String dataWydaniaStr = request.getParameter("dataWydania");
		String wydawnictwoIdStr =request.getParameter("wydawnictwoId");
		String cenaStr = request.getParameter("cena");
		String[] listaIdAutorow = request.getParameterValues("autorIdLista");
		
		try {
			List<Long> autorzyIdList = new ArrayList<>();
			KategoriaKsiazki kategoriaKsiazki = null;
			Wydawnictwo wydawnictwo = null;
			Date dataWydania = new SimpleDateFormat("yyyy-MM-dd").parse(dataWydaniaStr);
			
			Double cena = Double.parseDouble(cenaStr);
			
			if (kategoria != null && kategoria.length() > 0) {
				kategoriaKsiazki = KategoriaKsiazki.valueOf(kategoria);
			}
			if(wydawnictwoIdStr != null && wydawnictwoIdStr.length() > 0) {
				Long wydawnictwoId = Long.parseLong(wydawnictwoIdStr);
				wydawnictwo = wydawnictwoService.znajdzPoId(wydawnictwoId);
			}
			
			if (listaIdAutorow != null && listaIdAutorow.length > 0) {
				
				for(String idStr : listaIdAutorow) {
					autorzyIdList.add(Long.parseLong(idStr));
				}
			}
			
			Ksiazka ksiazka = new Ksiazka();
			ksiazka.setKategoria(kategoriaKsiazki);
			ksiazka.setOpis(opis);
			ksiazka.setTytul(tytul);
			ksiazka.setCena(cena);
			ksiazka.setDataWydania(dataWydania);
			ksiazka.setWydawnictwo(wydawnictwo);
			
			ksiazkaService.dodaj(ksiazka, autorzyIdList);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		response.sendRedirect("/ksiegarnia/crud/ksiazka/lista.htm");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
