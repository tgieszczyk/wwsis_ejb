package pl.wwsis.servlets.crud.ksiazka;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.w3.jaxws.services.KsiazkaService;
import pl.wwsis.encje.model.KsiazkaInfo;

/**
 */
@WebServlet("/crud/ksiazka/lista.htm")
public class ListaKsiazekServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private KsiazkaService ksiazkaService;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<KsiazkaInfo> listaKsiazek = ksiazkaService.znajdzWszystkieKsiazkiInfo();
		
		request.getSession().setAttribute("listaKsiazek", listaKsiazek);
		
		response.sendRedirect("/ksiegarnia/crud/ksiazka/lista-ksiazek.jsp");
	}

}
