package pl.wwsis.servlets.crud;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.services.Kalkulator;

/**
 * Servlet implementation class KalkulatorDodaj
 */
@WebServlet("/crud/kalkulator/dodaj.htm")
public class KalkulatorDodaj extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private Kalkulator kalkulator;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KalkulatorDodaj() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String aStr = request.getParameter("a");
		String bStr = request.getParameter("b");

		double a = Double.parseDouble(aStr);
		double b = Double.parseDouble(bStr);
		
		
		double wynik = kalkulator.dodaj(a, b);
		
		response.getWriter().append(wynik + "");
	}

}
