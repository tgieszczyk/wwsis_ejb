package pl.wwsis.servlets.crud;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class ZapiszServlet
 */
@WebServlet("/crud/zapisz.htm")
public class ZapiszServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private EntityManager em;

	@Override
	public void init(ServletConfig config) throws ServletException {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("orm-unit-postgresql");
		em = factory.createEntityManager();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		response.setCharacterEncoding("UTF-8");
		response.getWriter().append("Zapisałem " + em);
	}

}
