package pl.wwsis.servlets.crud.autor;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.w3.jaxws.services.AutorService;
import pl.wwsis.encje.Autor;

/**
 * Servlet implementation class ZapiszAutorServlet
 */
@WebServlet("/crud/autor/lista.htm")
public class ListaAutorowServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private AutorService autorService;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Autor> autorzy = autorService.znajdzWszystkich();
		
		request.getSession().setAttribute("listaAutorow", autorzy);
		
		response.sendRedirect("/ksiegarnia/crud/autor/lista-autorow.jsp");
	}

}
