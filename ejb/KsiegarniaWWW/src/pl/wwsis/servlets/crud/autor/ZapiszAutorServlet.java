package pl.wwsis.servlets.crud.autor;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.w3.jaxws.services.AutorService;
import pl.wwsis.encje.Autor;
import pl.wwsis.encje.repositories.AutorRepository;

/**
 * Servlet implementation class ZapiszAutorServlet
 */
@WebServlet("/crud/autor/zapisz.htm")
public class ZapiszAutorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private AutorService autorService;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String imie = request.getParameter("imie");
		String nazwisko = request.getParameter("nazwisko");
		
		autorService.dodajLubZapisz(new Autor(imie, nazwisko));
		
		response.sendRedirect("/ksiegarnia/crud/autor/lista.htm");
	}

}
