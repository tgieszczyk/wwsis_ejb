package pl.wwsis.ejb.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "AUTOR")
public class Autor extends BazowaEncja {

	private static final long serialVersionUID = 140101556646056939L;

	@Column(name = "IMIE")
	private String imie;
	@Column(name = "NAZWISKO")
	private String nazwisko;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "KSIAZKA_AUTOR", joinColumns = { @JoinColumn(name = "AUTOR_ID", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(referencedColumnName = "id", name = "KSIAZKA_ID") })
	private List<Ksiazka> ksiazki = new ArrayList<Ksiazka>();

	@ManyToMany(mappedBy = "autorzy", cascade = CascadeType.ALL)
	private List<Wydawnictwo> wydawnictwa = new ArrayList<Wydawnictwo>();

	public Autor() {
	}

	public Autor(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public List<Ksiazka> getKsiazki() {
		if (ksiazki == null) {
			ksiazki = new ArrayList<Ksiazka>();
		}
		return ksiazki;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public List<Wydawnictwo> getWydawnictwa() {
		if (wydawnictwa == null) {
			wydawnictwa = new ArrayList<Wydawnictwo>();
		}
		return wydawnictwa;
	}

}
