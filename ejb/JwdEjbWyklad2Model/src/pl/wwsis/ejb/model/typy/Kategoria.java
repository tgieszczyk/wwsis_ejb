package pl.wwsis.ejb.model.typy;

public enum Kategoria {
	SENSACJA, HISTORIA, MILOSNE_OPOWIASTKI, EPOPEJA, POWIESC;
}
