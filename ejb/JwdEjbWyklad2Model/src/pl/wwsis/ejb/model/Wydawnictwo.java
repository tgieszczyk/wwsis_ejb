package pl.wwsis.ejb.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "WYDAWNICTWO")
public class Wydawnictwo extends BazowaEncja {

	private static final long serialVersionUID = 140101556646056939L;

	@Column(name = "NAZWA")
	private String nazwa;

	@OneToMany(mappedBy = "wydawnictwo")
	private List<Ksiazka> ksiazki = new ArrayList<Ksiazka>();

	@ManyToMany
	@JoinTable(name = "WYDAWNICTWO_AUTOR", joinColumns = { @JoinColumn(name = "AUTOR_ID", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(referencedColumnName = "id", name = "WYDAWNICTWO_ID") })
	private List<Autor> autorzy = new ArrayList<Autor>();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ADRES_SIEDZIBA_ID")
	private Adres adres;

	public Wydawnictwo() {
		// TODO Auto-generated constructor stub
	}

	public Wydawnictwo(String nazwa, Adres adres) {
		this.nazwa = nazwa;
		this.adres = adres;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public List<Ksiazka> getKsiazki() {
		if (ksiazki == null) {
			ksiazki = new ArrayList<Ksiazka>();
		}
		return ksiazki;
	}

	public List<Autor> getAutorzy() {
		if (autorzy == null) {
			autorzy = new ArrayList<Autor>();
		}
		return autorzy;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

}
