package pl.wwsis.ejb.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class BazowaEncja implements BaseEntity<Long> {

	private static final long serialVersionUID = 725675412833703696L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Long id;

	@Column(name = "DATA_OSTATNIEJ_MODYFIKACJI")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataOstatniejModyfikacji;

	public Date getDataOstatniejModyfikacji() {
		return dataOstatniejModyfikacji;
	}

	public void setDataOstatniejModyfikacji(Date dataOstatniejModyfikacji) {
		this.dataOstatniejModyfikacji = dataOstatniejModyfikacji;
	}

	public Long getId() {
		return id;
	}
}
