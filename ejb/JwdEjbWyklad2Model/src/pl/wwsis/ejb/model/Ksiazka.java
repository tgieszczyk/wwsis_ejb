package pl.wwsis.ejb.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import pl.wwsis.ejb.model.typy.Kategoria;

@Entity
@Table(name = "KSIAZKA")
@NamedQueries({ @NamedQuery(name = "ksiazka.updateCeny", query = "UPDATE Ksiazka k SET k.cena = k.cena + :delta") })
public class Ksiazka extends BazowaEncja {

	private static final long serialVersionUID = -5005541634015978509L;

	@Column(name = "TYTUL")
	private String tytul;
	@Column(name = "OPIS")
	@Lob
	private String opis;
	@Temporal(TemporalType.DATE)
	@Column(name = "DATA_WYDANIA")
	private Date dataWydania;
	@Column(name = "CENA")
	private Double cena;

	@Enumerated
	@Column(name = "KATEGORIA")
	private Kategoria kategoria;

	@ManyToMany(mappedBy = "ksiazki", cascade = { CascadeType.PERSIST,
			CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH })
	private List<Autor> autorzy = new ArrayList<Autor>();

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.DETACH,
			CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "WYDAWNICTWO_ID")
	private Wydawnictwo wydawnictwo;

	public Ksiazka() {
	}

	public Ksiazka(String tytul, String opis, Date dataWydania, Double cena,
			Kategoria kategoria, Wydawnictwo wydawnictwo) {
		this.tytul = tytul;
		this.opis = opis;
		this.dataWydania = dataWydania;
		this.cena = cena;
		this.kategoria = kategoria;
		this.wydawnictwo = wydawnictwo;
	}

	public List<Autor> getAutorzy() {
		if (autorzy == null) {
			autorzy = new ArrayList<Autor>();
		}
		return autorzy;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Date getDataWydania() {
		return dataWydania;
	}

	public void setDataWydania(Date dataWydania) {
		this.dataWydania = dataWydania;
	}

	public Kategoria getKategoria() {
		return kategoria;
	}

	public void setKategoria(Kategoria kategoria) {
		this.kategoria = kategoria;
	}

	public Wydawnictwo getWydawnictwo() {
		return wydawnictwo;
	}

	public void setWydawnictwo(Wydawnictwo wydawnictwo) {
		this.wydawnictwo = wydawnictwo;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

}
