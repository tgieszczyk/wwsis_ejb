package pl.wwsis.encje.model;

import java.io.Serializable;
import java.util.Date;

import pl.wwsis.encje.typy.KategoriaKsiazki;

/**
 * @author a051213
 *
 */
public class KsiazkaInfo implements Serializable {
	private static final long serialVersionUID = -6202164960686248393L;
	
	private String tytul;
	private String nazwaWydawnictwa;
	private String opis;
	private KategoriaKsiazki kategoria;
	private Date dataWydania;
	private Double cena;

	public KsiazkaInfo(String tytul, String nazwaWydawnictwa, String opis,
			KategoriaKsiazki kategoria, Date dataWydania, Double cena) {
		this.tytul = tytul;
		this.nazwaWydawnictwa = nazwaWydawnictwa;
		this.opis = opis;
		this.kategoria = kategoria;
		this.dataWydania = dataWydania;
		this.cena = cena;
	}

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public String getNazwaWydawnictwa() {
		return nazwaWydawnictwa;
	}

	public void setNazwaWydawnictwa(String nazwaWydawnictwa) {
		this.nazwaWydawnictwa = nazwaWydawnictwa;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public KategoriaKsiazki getKategoria() {
		return kategoria;
	}

	public void setKategoria(KategoriaKsiazki kategoria) {
		this.kategoria = kategoria;
	}

	public Date getDataWydania() {
		return dataWydania;
	}

	public void setDataWydania(Date dataWydania) {
		this.dataWydania = dataWydania;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

}
