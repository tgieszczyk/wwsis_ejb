package pl.wwsis.encje.model;

import java.io.Serializable;

/**
 * @author a051213
 *
 */
public class WydawnictwoInfo implements Serializable {
	private long id;
	private String nazwa;

	public WydawnictwoInfo(long id, String nazwa) {
		this.id = id;
		this.nazwa = nazwa;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

}
