package pl.wwsis.encje.typy;

/**
 * @author ORM
 *
 */
public enum KategoriaKsiazki {
	HORROR("Horror"), SENSACYJNE("Sensacyjne"), SCIFI("Sci-fi"), POPULARNO_NAUKOWE(
			"Popularno-naukowe"), BAJKI("Bajki"), KOMEDIA("Komedia"), ROMANTICO("Love story"),
			KOMPUTEROWE("Informatyczne");

	private final String label;

	private KategoriaKsiazki(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
