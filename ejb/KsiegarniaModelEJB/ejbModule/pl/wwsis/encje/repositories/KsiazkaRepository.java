package pl.wwsis.encje.repositories;

import java.util.Arrays;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.model.KsiazkaInfo;
import pl.wwsis.encje.typy.KategoriaKsiazki;

/**
 * Session Bean implementation class KsiazkaRepository
 */
// stateless -> bezstanowy
@Stateless
@LocalBean
// dostepny z poziomu aplicji nie serwera
public class KsiazkaRepository {

	// Dependency Injection (DI)
	// mechanizm do tworznia
	@PersistenceContext(unitName = "orm-unit")
	private EntityManager em;

	// tez DI
	// @PersistenceUnit(unitName = "orm-unit")
	// private EntityManagerFactory emFactory;

	public List<Ksiazka> znajdzPoLiczbieAutorow(int liczbaAutorow) {
		Query q = em
				.createNamedQuery(Ksiazka.NAMED_QUERY_ZANJDZ_KSIAZLI_PO_LICZBIE_AUTOROW);
		q.setParameter("liczbaAutorow", liczbaAutorow);
		return q.getResultList();
	}

	public List<Ksiazka> znajdzPoKategorii(KategoriaKsiazki... kategorie) {
		Query q = em.createNamedQuery(Ksiazka.NAMED_QUERY_ZNAJDZ_PO_KATEGORII);
		q.setParameter("kategorie", Arrays.asList(kategorie));
		return q.getResultList();
	}

	public List<KsiazkaInfo> znajdzWszystkieKsiazkiInfo() {
		Query q = em.createNamedQuery(Ksiazka.NAMED_QUERY_ZNAJDZ_WSZYSTKIE_INFORMACJE);
		return q.getResultList();
	}
	
	public void dodajLubZapisz(Ksiazka k) {
		em.persist(k);
	}
}
