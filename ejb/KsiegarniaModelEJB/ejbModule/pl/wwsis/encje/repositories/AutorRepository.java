package pl.wwsis.encje.repositories;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;

import pl.wwsis.encje.Autor;

@Stateless
@LocalBean
public class AutorRepository {
	// Dependency Injection (DI)
	// mechanizm do tworznia
	@PersistenceContext(unitName = "orm-unit")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Autor> znajdzPoTytule(String tytul) {
		return em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_PO_TYTULE_KSIAZKI)
				.setParameter("tytul", tytul).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Autor> znajdzPoMinLiczbieKsiazek(int minLiczbaKsiazek) {
		return em
				.createNamedQuery(
						Autor.NAMED_QUERY_ZNAJD_O_MIN_LICZBIE_WYDANYCH_KSIAZEK)
				.setParameter("minLiczbaKsiazek", minLiczbaKsiazek)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Autor> znajdzWszystkich() {
		return em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_WSZYSTKICH)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Autor> znajdzWszystkichPoId(List<Long> listaId) {
		return em.createNamedQuery(Autor.NAMED_QUERY_ZNAJD_WSZYSTKICH_PO_ID)
				.setParameter("listaId", listaId).getResultList();
	}

	public void zapisz(Autor autor) {
		em.persist(autor);
	}
}
