package pl.wwsis.encje.repositories;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.model.WydawnictwoInfo;

/**
 * Session Bean implementation class KsiazkaRepository
 */
// stateless -> bezstanowy
@Stateless
@LocalBean
// dostepny z poziomu aplicji nie serwera
public class WydawnictwoRepository {

	// Dependency Injection (DI)
	// mechanizm do tworznia
	@PersistenceContext(unitName = "orm-unit")
	private EntityManager em;

	public List<WydawnictwoInfo> znajdzWszystkieWydawnictwoInfo() {
		Query q = em
				.createNamedQuery(Wydawnictwo.NAMED_QUERY_ZNAJDZ_WSZYSTKIE_INFORMACJE);
		return q.getResultList();
	}
	
	public Wydawnictwo znajdzPoId(Long id) {
		return em.find(Wydawnictwo.class, id);
	}
}
