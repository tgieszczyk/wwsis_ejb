package pl.wwsis.ejb.wyklad2.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.wyklad2.przyklad01.HelloWoldLocal;
import pl.wwsis.ejb.wyklad2.przyklad01.HelloWorldBeanLocal;

/**
 * Servlet implementation class KlientViewServlet
 */
@WebServlet({ "/wyklad2/hello-world" })
public class HelloWorldServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private HelloWorldBeanLocal beanLocal;

	@EJB
	private HelloWoldLocal helloWoldLocal;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		if (name == null || name.length() == 0) {
			response.getWriter()
					.append("Brak parametru wej�ciowego 'name', wprowadz go jeszcze raz: <a href=\"hello-world.jsp\" >hello-world.jsp</a>");
			return;
		}
		String helloMessage = beanLocal.getHelloMessage(name);
		String helloMessageLocal = helloWoldLocal.getHelloMessage(name);

		response.getWriter()
				.append(beanLocal.getClass().getName())
				.append(": ")
				.append(helloMessage)
				.append("<br/>")
				.append(helloWoldLocal.getClass().getName())
				.append(": ")
				.append(helloMessageLocal)
				.append("<br/>")
				.append("Przywitaj si� jeszcze raz: <a href=\"hello-world.jsp\" >hello-world.jsp</a>");
	}

}
