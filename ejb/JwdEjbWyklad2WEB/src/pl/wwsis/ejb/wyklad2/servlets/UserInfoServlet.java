package pl.wwsis.ejb.wyklad2.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.wwsis.ejb.wyklad2.przyklad02.UserInfoSingletonBean;
import pl.wwsis.ejb.wyklad2.przyklad02.UserInfoStatefullBean;
import pl.wwsis.ejb.wyklad2.przyklad02.UserInfoStatelessBean;

/**
 * Servlet implementation class Step1FirstName
 */
@WebServlet("/wyklad2/user-info")
public class UserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private UserInfoStatefullBean userInfoBean;
	@EJB
	private UserInfoStatelessBean statelessBean;
	@EJB
	private UserInfoSingletonBean singletonBean;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String age = request.getParameter("age");
		userInfoBean.setFirstName(name);
		userInfoBean.setLastName(lastName);
		userInfoBean.setAge(age);

		statelessBean.setFirstName(name);
		statelessBean.setLastName(lastName);
		statelessBean.setAge(age);

		singletonBean.setFirstName(name);
		singletonBean.setLastName(lastName);
		singletonBean.setAge(age);

		response.getWriter().append(userInfoBean.getFullInfo());

		// getServletContext().getRequestDispatcher("/wyklad2/user-info.jsp").forward(request,
		// response);
	}
}
