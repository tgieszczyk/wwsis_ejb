package pl.wwsis.ejb.wyklad2.przyklad01;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class HelloWorldBean
 */
@Stateless
@LocalBean
public class HelloWorldBean1 implements HelloWorldBeanLocal {

	private static final String HELLO_MSG_TEMPLATE = "Witaj %s w swiecie EJB (3.1)";

	@Override
	public String getHelloMessage(String name) {
		return String.format(HELLO_MSG_TEMPLATE, name);
	}

}
