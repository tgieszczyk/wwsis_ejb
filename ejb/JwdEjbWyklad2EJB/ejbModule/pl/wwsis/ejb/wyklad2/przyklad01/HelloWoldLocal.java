package pl.wwsis.ejb.wyklad2.przyklad01;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class HelloWoldLocal
 */
@Stateless
@LocalBean
public class HelloWoldLocal {
	@EJB
	private HelloWorldBeanLocal delegate;

	public String getHelloMessage(String name) {
		return "Delegate message: " + delegate.getHelloMessage(name);
	}

}
