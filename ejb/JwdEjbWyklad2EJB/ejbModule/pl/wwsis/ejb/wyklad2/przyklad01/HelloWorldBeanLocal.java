package pl.wwsis.ejb.wyklad2.przyklad01;

import javax.ejb.Local;

@Local
public interface HelloWorldBeanLocal {
	String getHelloMessage(String name);
}
