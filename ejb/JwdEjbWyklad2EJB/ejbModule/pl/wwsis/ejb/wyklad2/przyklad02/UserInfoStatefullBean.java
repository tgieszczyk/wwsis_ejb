package pl.wwsis.ejb.wyklad2.przyklad02;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

/**
 * Session Bean implementation class CounterBean
 */
@Stateful
@LocalBean
public class UserInfoStatefullBean implements Serializable {
	private static int index = 0;
	private static final long serialVersionUID = 1815040461568058798L;
	private String firstName;
	private String lastName;
	private String age;

	{
		index++;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAge(String age) {
		this.age = age;
	}

//	@Remove
	public String getFullInfo() {
		return index + " : " + firstName + " " + lastName + ", age: " + age;
	}
}
