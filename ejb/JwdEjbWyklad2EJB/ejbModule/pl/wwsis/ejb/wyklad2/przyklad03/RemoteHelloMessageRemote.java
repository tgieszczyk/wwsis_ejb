package pl.wwsis.ejb.wyklad2.przyklad03;

import javax.ejb.Remote;

@Remote
public interface RemoteHelloMessageRemote {

	String getHelloMessage(String name);
}
