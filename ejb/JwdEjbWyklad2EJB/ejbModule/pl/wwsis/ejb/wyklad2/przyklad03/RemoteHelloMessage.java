package pl.wwsis.ejb.wyklad2.przyklad03;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import pl.wwsis.ejb.wyklad2.przyklad01.HelloWorldBeanLocal;

/**
 * Session Bean implementation class RemobeHelloMessage
 */
@Stateless
@LocalBean
public class RemoteHelloMessage implements RemoteHelloMessageRemote {
	@EJB
	private HelloWorldBeanLocal helloWorldBeanLocal;

	@Override
	public String getHelloMessage(String name) {
		return "Remote delegate: " + helloWorldBeanLocal.getHelloMessage(name);
	}
}
