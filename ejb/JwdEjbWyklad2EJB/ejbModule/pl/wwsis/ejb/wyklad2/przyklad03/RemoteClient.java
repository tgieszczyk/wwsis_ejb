package pl.wwsis.ejb.wyklad2.przyklad03;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class RemoteClient {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
invokeStatelessBean();
	}
	
	private static void invokeStatelessBean() throws NamingException {
        // Let's lookup the remote stateless calculator
        final RemoteHelloMessageRemote remoteHelloMessageRemote = lookupRemoteHelloMessageRemote();
        System.out.println(remoteHelloMessageRemote.getHelloMessage("Remote client Tomasz"));
        
    }

	private static RemoteHelloMessageRemote lookupRemoteHelloMessageRemote() throws NamingException {
        final Hashtable<String, Object> jndiProperties = new Hashtable<String, Object>();
//        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        jndiProperties.put(Context.PROVIDER_URL, "remote://localhost:4447");
        jndiProperties.put("jboss.naming.client.ejb.context", true);
        final Context context = new InitialContext(jndiProperties);
        list(context);
        
        // The app name is the application name of the deployed EJBs. This is typically the ear name
        // without the .ear suffix. However, the application name could be overridden in the application.xml of the
        // EJB deployment on the server.
        // Since we haven't deployed the application as a .ear, the app name for us will be an empty string
        final String appName = "";
        // This is the module name of the deployed EJBs on the server. This is typically the jar name of the
        // EJB deployment, without the .jar suffix, but can be overridden via the ejb-jar.xml
        // In this example, we have deployed the EJBs in a jboss-as-ejb-remote-app.jar, so the module name is
        // jboss-as-ejb-remote-app
        final String moduleName = "JwdEjbWyklad2EAR";
        // AS7 allows each deployment to have an (optional) distinct name. We haven't specified a distinct name for
        // our EJB deployment, so this is an empty string
        final String distinctName = "";
        // The EJB name which by default is the simple class name of the bean implementation class
        final String beanName = RemoteHelloMessage.class.getSimpleName();
        // the remote view fully qualified class name
        final String viewClassName = RemoteHelloMessageRemote.class.getName();
        // let's do the lookup

//      return (RemoteHelloMessageRemote) context.lookup("ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName);
      return (RemoteHelloMessageRemote) context.lookup("JwdEjbWyklad2EAR/JwdEjbWyklad2EJB//RemoteHelloMessage!pl.wwsis.ejb.wyklad2.przyklad03.RemoteHelloMessageRemote");
    }
	public static void list(Context context) throws NamingException {
		NamingEnumeration<NameClassPair> list = context.list("");
		while (list.hasMore()) {
			NameClassPair pair = list.next();
			System.out.println(pair.getClassName());
		}
	}

}
