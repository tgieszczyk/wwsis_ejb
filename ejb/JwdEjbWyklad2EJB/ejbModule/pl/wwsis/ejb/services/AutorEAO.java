package pl.wwsis.ejb.services;

import javax.ejb.Local;

import pl.wwsis.ejb.model.Autor;

@Local
public interface AutorEAO {
	Autor findById(Long id);
}
