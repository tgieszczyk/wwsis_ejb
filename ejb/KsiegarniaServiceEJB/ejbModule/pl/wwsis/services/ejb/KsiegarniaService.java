package pl.wwsis.services.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import pl.wwsis.encje.Autor;
import pl.wwsis.encje.repositories.AutorRepository;
import pl.wwsis.encje.repositories.KsiazkaRepository;

/**
 * Session Bean implementation class KsiazkaBean
 */
@Stateless
@LocalBean
public class KsiegarniaService {

	// TEZ DI
	@EJB
	private KsiazkaRepository ksiazkaRepository;

	// TEZ DI
	@EJB
	private AutorRepository autorRepository;

	public List<Autor> znajdzWszystkich() {
		return autorRepository.znajdzWszystkich();
	}

}
