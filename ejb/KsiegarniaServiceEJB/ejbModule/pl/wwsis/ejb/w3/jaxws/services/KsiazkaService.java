package pl.wwsis.ejb.w3.jaxws.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;

import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.model.KsiazkaInfo;
import pl.wwsis.encje.repositories.KsiazkaRepository;

/**
 * Session Bean implementation class KsiazkaService
 */
@Singleton
@LocalBean
public class KsiazkaService {
	@EJB
	private AutorService autorService;
	@EJB
	private KsiazkaRepository ksiazkaRepository;

    /**
     * Default constructor. 
     */
    public KsiazkaService() {
        // TODO Auto-generated constructor stub
    }

    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void dodaj(Ksiazka ksiazka, List<Long> autorzyIdLista) {
//    	ksiazka.getAutorzy().addAll(autorService.znajdzWszystkichPoId(autorzyIdLista));
//    	ksiazkaRepository.dodajLubZapisz(ksiazka);
    }
    
    public List<KsiazkaInfo> znajdzWszystkieKsiazkiInfo() {
    	return ksiazkaRepository.znajdzWszystkieKsiazkiInfo();
    }
}
