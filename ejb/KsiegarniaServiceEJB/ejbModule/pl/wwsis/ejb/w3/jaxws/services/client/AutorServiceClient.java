package pl.wwsis.ejb.w3.jaxws.services.client;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.ws.WebServiceRef;

import pl.wwsis.ejb.w3.jaxws.services.AutorServiceWS;
import pl.wwsis.encje.Autor;

@Stateless
@LocalBean
public class AutorServiceClient {

	@WebServiceRef(wsdlLocation = "META-INF/wsdl/autorzy.wsdl", name = "SerwisAutorow")
	private AutorServiceWS autorService;

	public List<Autor> wszyscyAutorzy() {
		return autorService.znajdzWszystkich();
	}

	public void zapiszAutora(String imie, String nazwisko) {
		autorService.dodajLubZapisz(imie, nazwisko);
	}

}
