package pl.wwsis.ejb.w3.jaxws.services;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import pl.wwsis.encje.Autor;
import pl.wwsis.encje.repositories.AutorRepository;

/**
 */
@Stateless
@WebService(targetNamespace = "http://ejb.wwsis.pl/jaxws/model", 
name = "SerwisAutorow", 
serviceName = "AutorServiceWS", 
portName = "AutorServicePort")
public class AutorServiceWS {

	@EJB
	private AutorRepository autorRepository;

	@WebMethod(operationName = "znajdzAutorow", action = "znajdzAll")
	public List<Autor> znajdzWszystkich() {
		return autorRepository.znajdzWszystkich();
	}

	@WebMethod(operationName = "dodajAutora", action = "dodajJednego")
	public void dodajLubZapisz(
			@WebParam(name = "imieAutora", header = true) String imie,
			@WebParam(name = "nazwiskoAutora") String nazwisko) {
		autorRepository.zapisz(new Autor(imie, nazwisko));
	}

	@WebMethod(operationName = "znajdzWgParametrow", action = "znajdzWgParametrow")
	public List<Autor> znajdzWgParametrow(ParametrAutorow param) {
		Autor result = new Autor("Tomasz", "Gieszczyk");
		return Arrays.asList(result);
	}
}
