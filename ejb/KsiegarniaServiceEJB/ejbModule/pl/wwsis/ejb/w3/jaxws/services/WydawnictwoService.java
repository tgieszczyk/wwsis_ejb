package pl.wwsis.ejb.w3.jaxws.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;

import pl.wwsis.encje.Autor;
import pl.wwsis.encje.Ksiazka;
import pl.wwsis.encje.Wydawnictwo;
import pl.wwsis.encje.model.KsiazkaInfo;
import pl.wwsis.encje.model.WydawnictwoInfo;
import pl.wwsis.encje.repositories.KsiazkaRepository;
import pl.wwsis.encje.repositories.WydawnictwoRepository;

/**
 * Session Bean implementation class KsiazkaService
 */
@Singleton
@LocalBean
public class WydawnictwoService {
	@EJB
	private WydawnictwoRepository wydawnictwoRepository;

	public List<WydawnictwoInfo> znajdzWszystkieWydawnictwoInfo() {
		return wydawnictwoRepository.znajdzWszystkieWydawnictwoInfo();
	}
	
	public Wydawnictwo znajdzPoId(Long id) {
		return wydawnictwoRepository.znajdzPoId(id);
	}
}
