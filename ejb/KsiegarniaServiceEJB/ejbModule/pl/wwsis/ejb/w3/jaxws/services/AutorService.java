package pl.wwsis.ejb.w3.jaxws.services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import pl.wwsis.encje.repositories.AutorRepository;
import pl.wwsis.encje.Autor;

/**
 * Session Bean implementation class AutorService
 */
@Singleton
@LocalBean
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AutorService {
	
	@EJB
	private AutorRepository autorRepository;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void dodajLubZapisz(Autor... autorzy) {
		for(Autor autor : autorzy) {
			autorRepository.zapisz(autor);
		}
	}
	
	public List<Autor> znajdzWszystkich() {
		return autorRepository.znajdzWszystkich();
	}
	

	
	public List<Autor> znajdzWszystkichPoId(List<Long> listaId) {
		return autorRepository.znajdzWszystkichPoId(listaId);
	}
}
