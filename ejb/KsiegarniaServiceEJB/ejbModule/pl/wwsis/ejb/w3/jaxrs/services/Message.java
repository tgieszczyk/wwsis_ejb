package pl.wwsis.ejb.w3.jaxrs.services;

public class Message {
	private MessageType type = MessageType.INFO;
	private String message;

	public Message(MessageType type, String message) {
		this.type = type;
		this.message = message;
	}

	public Message(String message) {
		this.message = message;
	}

	public Message() {
		// TODO Auto-generated constructor stub
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
