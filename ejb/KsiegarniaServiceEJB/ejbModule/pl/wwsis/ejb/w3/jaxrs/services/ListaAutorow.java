package pl.wwsis.ejb.w3.jaxrs.services;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import pl.wwsis.encje.Autor;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ListaAutorow {
	@XmlElement
	private List<Autor> autorzy;
	private List<Message> messages;

	public List<Autor> getAutorzy() {
		return autorzy;
	}

	public void setAutorzy(List<Autor> autorzy) {
		this.autorzy = autorzy;
	}

	public List<Message> getMessages() {
		if (messages == null) {
			messages = new ArrayList<Message>();
		}
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

}
