package pl.wwsis.ejb.w3.jaxrs.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;

import pl.wwsis.ejb.services.AutorRepository;
import pl.wwsis.encje.Autor;

/**
 * Session Bean implementation class UzytkownicyService
 */
@Stateless
@LocalBean
@Path("/autorzy")
public class AutorRSService {

	@EJB
	private AutorRepository autorRepository;

	// @GET
	// @Path("/fetch.ajax")
	// @Produces({ "application/json" })
	// public Response wszyscyAutorzy() {
	// List<Autor> result = autorRepository.znajdzWszystkich();
	//
	// final ListaAutorow autorzy = new ListaAutorow();
	// autorzy.setAutorzy(result);
	//
	// return getJsonResponse(autorzy);
	// }

	@GET
	@Path("/fetch.ajax")
	@Produces({ "application/json" })
	public Response wszyscyAutorzy() {
		List<Autor> result = autorRepository.znajdzWszystkich();

		final ListaAutorow autorzy = new ListaAutorow();
		autorzy.setAutorzy(result);

		return getJsonResponse(autorzy);
	}

	@POST
	@Produces({ "application/json" })
	// @Consumes({ "application/x-www-form-urlencoded", "application/json" })
	@Consumes({ "application/json" })
	public Response save(InputStream inputStream) {
		ListaAutorow result = new ListaAutorow();
		try {
			Autor autor = getObjectFromJson(inputStream, Autor.class);

			autorRepository.zapisz(autor);

			result.getMessages().add(
					new Message("Autor został pomyślnie dodany"));

			List<Autor> lista = autorRepository.znajdzWszystkich();

			result.setAutorzy(lista);
		} catch (Exception e) {
			e.printStackTrace();
			result.getMessages().add(
					new Message(MessageType.ERROR, e.getMessage()));
		}

		return getJsonResponse(result);
	}

	// @GET
	// @Path("save/{imie}/{nazwisko}.ajax")
	// @Produces({ "application/json" })
//	 public Autor zapiszAutora(@PathParam("imie") String imie,
	// @PathParam("nazwisko") String nazwisko) {
	// autorRepository.zapisz(imie, nazwisko);
	// return new Autor(imie, nazwisko);
	// }

	private Response getJsonResponse(final Object object) {
		return Response.ok(new StreamingOutput() {
			@Override
			public void write(OutputStream paramOutputStream)
					throws IOException, WebApplicationException {
				ObjectMapper mapper = new ObjectMapper();
//				mapper.configure(Feature.INDENT_OUTPUT, true);
				String json = mapper.writeValueAsString(object);
				paramOutputStream.write(json.getBytes());
			}
		}).build();
	}

	private <T> T getObjectFromJson(final InputStream is, Class<T> clazz)
			throws IOException, WebApplicationException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(is, clazz);
	}
}
