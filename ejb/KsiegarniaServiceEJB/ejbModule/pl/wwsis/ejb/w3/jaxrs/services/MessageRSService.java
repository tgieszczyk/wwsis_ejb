package pl.wwsis.ejb.w3.jaxrs.services;

import java.io.IOException;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.codehaus.jackson.map.ObjectMapper;

import pl.wwsis.ejb.mdb.p001.MessageSenderBean;

/**
 * Session Bean implementation class UzytkownicyService
 */
@Stateless
@LocalBean
@Path("/messages")
public class MessageRSService {

	@EJB
	private MessageSenderBean messageSender;

	// @QueryParam to parametr url
	// przyklad: http://<servier>/<app>/resources-rs/messages/send.ajax?message=<parametr>
	// @PathParam to parametr bedacy czescia URLa
	// przyklad: http://<servier>/<app>/resources-rs/messages/send.ajax/{message} -> {message} to path param
	@GET
	@Path("/send.ajax")
	@Produces({ "application/json" })
	public Response wyslijWiadomosc(@QueryParam("message") String message) {
		messageSender.sendTextMessage(message);

		Message msg = new Message("Przekazane to przetworzenia");
		return getJsonResponse(msg);
	}


	private Response getJsonResponse(final Object object) {
		return Response.ok(new StreamingOutput() {
			@Override
			public void write(OutputStream paramOutputStream)
					throws IOException, WebApplicationException {
				ObjectMapper mapper = new ObjectMapper();
//				mapper.configure(Feature.INDENT_OUTPUT, true);
				String json = mapper.writeValueAsString(object);
				paramOutputStream.write(json.getBytes());
			}
		}).build();
	}

}
