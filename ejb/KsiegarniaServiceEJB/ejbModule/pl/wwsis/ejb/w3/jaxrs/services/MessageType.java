package pl.wwsis.ejb.w3.jaxrs.services;

public enum MessageType {
	ERROR, WARNING, INFO;
}
