package pl.wwsis.ejb.mdb.p001;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;

/**
 * Session Bean implementation class MessageSenderBean
 */
@Stateless
@LocalBean
public class MessageSenderBean {

	// @Resource(name = "java:/ConnectionFactory")
	@Resource(name = "java:/JmsXA")
	private ConnectionFactory connectionFactory;
	@Resource(name = "java:/topic/consumer1")
	private Destination helloTopic;
	@Resource(name = "java:/topic/consumer2")
	private Destination helloTopic2;
	@Resource(name = "java:/queue/consumer1")
	private Destination helloQueue;
	@Resource(name = "java:/queue/test")
	private Destination testow1;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendTextMessage(String message) {
		// utworzenie połączenia
		try {
			Connection connection = connectionFactory.createConnection();
			Session session = connection.createSession(true,
					Session.AUTO_ACKNOWLEDGE);

			javax.jms.MessageProducer producer = session
					.createProducer(helloTopic2);
			connection.start();
			try {
				TextMessage textMsg = session.createTextMessage();
				textMsg.setIntProperty("STATUS", 1);
				// StreamMessage m = session.createStreamMessage();
				// m.setBooleanProperty("isStream", true);
				textMsg.setText(message);
				producer.send(textMsg);
			} finally {
				session.close();
				connection.close();
			}
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			System.out.println("Wystlane do przetwarzania");
		}
	}

	// @TransactionAttribute(TransactionAttributeType.REQUIRED)
	// public void sendTextMessage(String message) {
	// // utworzenie połączenia
	// try {
	// QueueConnection connection = connectionFactory.createQueueConnection();
	// QueueSession session = connection.createQueueSession(false,
	// Session.AUTO_ACKNOWLEDGE);
	//
	// QueueSender sender = session.createSender(helloQueue);
	//
	//
	// // javax.jms.MessageProducer producer = session
	// // .createProducer(helloQueue);
	// // connection.start();
	//
	// TextMessage textMsg = session.createTextMessage();
	// textMsg.setText(message);
	// sender.send(textMsg);
	//
	// session.close();
	// connection.close();
	// } catch (JMSException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
}
