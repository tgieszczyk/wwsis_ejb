package pl.wwsis.ejb.mdb.p002;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/topic/consumer1") })
public class HelloTopicConsumer1 implements MessageListener {
	public void onMessage(Message message) {
		System.out.println(getClass().getSimpleName() + " - " + message);
	}

}
