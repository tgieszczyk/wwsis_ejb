package pl.wwsis.ejb.mdb.p002;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.StreamMessage;

/**
 * Message-Driven Bean implementation class for: HelloConsumer
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/queue/myStreamQueue"),
		@ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "isStream = true"), })
public class StreamQueueConsumer implements MessageListener {
	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message) {
		try {
			StreamMessage streamMessage = (StreamMessage) message;

			FileOutputStream fos = null;
			BufferedOutputStream bos = null;

			bos = new BufferedOutputStream(fos = new FileOutputStream(
					"c:\\jwd\\jmsoutput\\received"
							+ message.getStringProperty("suffix")));

			byte[] b = new byte[2048];
			int len = 0;

			while ((len = streamMessage.readBytes(b)) != -1) {
				bos.write(b, 0, len);

			}
			bos.flush();
			bos.close();
			fos.close();

			System.out.println(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
