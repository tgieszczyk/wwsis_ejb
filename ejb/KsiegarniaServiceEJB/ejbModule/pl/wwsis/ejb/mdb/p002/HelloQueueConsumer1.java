package pl.wwsis.ejb.mdb.p002;

import java.util.Enumeration;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Message-Driven Bean implementation class for: HelloConsumer
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/queue/consumer1") }, mappedName = "QConsumer1")
public class HelloQueueConsumer1 implements MessageListener {
	/**
	 * @see MessageListener#onMessage(Message)
	 */
	@SuppressWarnings("unchecked")
	public void onMessage(Message message) {
		try {
			Enumeration<String> propertyNames = message.getPropertyNames();
			while (propertyNames.hasMoreElements()) {
				System.out.println(propertyNames.nextElement());
			}
			if (message instanceof TextMessage) {
				System.out.println(((TextMessage) message).getText());
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
