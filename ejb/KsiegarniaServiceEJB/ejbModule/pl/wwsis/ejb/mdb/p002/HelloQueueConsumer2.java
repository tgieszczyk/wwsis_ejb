package pl.wwsis.ejb.mdb.p002;

import java.util.Enumeration;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

/**
 * Message-Driven Bean implementation class for: HelloConsumer
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/queue/consumer2") })
public class HelloQueueConsumer2 implements MessageListener {
	/**
	 * @see MessageListener#onMessage(Message)
	 */
	@SuppressWarnings("unchecked")
	public void onMessage(Message message) {

		try {
			Enumeration<String> propertyNames = message.getPropertyNames();

			while (propertyNames.hasMoreElements()) {
				System.out.println(propertyNames.nextElement());
			}
			System.out.println(getClass().getSimpleName() + " - " + message);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
