package pl.wwsis.ejb.model;

public class UserContext {

	private static String zalogowanyUzytkownik = "AUTOMAT";
	static {
		String user = System.getProperty("user.name");
		if (user != null && user.length() > 0) {
			zalogowanyUzytkownik = user;
		}
	}

	public static String getZalogowanyUzytkownik() {
		return zalogowanyUzytkownik;
	}

	public static void setZalogowanyUzytkownik(String zalogowanyUzytkownik) {
		UserContext.zalogowanyUzytkownik = zalogowanyUzytkownik;
	}

}
