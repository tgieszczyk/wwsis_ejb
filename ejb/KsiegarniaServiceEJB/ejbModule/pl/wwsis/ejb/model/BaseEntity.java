package pl.wwsis.ejb.model;

import java.io.Serializable;
import java.util.Date;

public interface BaseEntity<ID extends java.io.Serializable> extends
		Serializable {
	ID getId();

	Date getDataOstatniejModyfikacji();
}
