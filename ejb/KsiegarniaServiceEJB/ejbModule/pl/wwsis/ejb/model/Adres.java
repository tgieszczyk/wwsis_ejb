package pl.wwsis.ejb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ADRES")
public class Adres extends BazowaEncja {

	private static final long serialVersionUID = 140101556646056939L;


	@Column(name = "ULICA")
	private String ulica;
	@Column(name = "KOD_POCZTOWY")
	private String kodPocztowy;
	@Column(name = "MIASTO")
	private String miasto;
	@Column(name = "KRAJ")
	private String kraj;

	public Adres() {
	}
	

	public Adres(String ulica, String kodPocztowy, String miasto, String kraj) {
		this.ulica = ulica;
		this.kodPocztowy = kodPocztowy;
		this.miasto = miasto;
		this.kraj = kraj;
	}


	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public String getMiasto() {
		return miasto;
	}

	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}

	public String getKraj() {
		return kraj;
	}

	public void setKraj(String kraj) {
		this.kraj = kraj;
	}

	
}
