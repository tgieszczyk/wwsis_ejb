package pl.wwsis.ejb.services.przyklad3;

import javax.ejb.Local;

@Local
public interface SelfInjectedBeanLocal {
	String name();
	String generateNewName();
}
