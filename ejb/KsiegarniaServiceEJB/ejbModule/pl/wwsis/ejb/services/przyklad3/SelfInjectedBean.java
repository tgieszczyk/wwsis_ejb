package pl.wwsis.ejb.services.przyklad3;

import java.util.Random;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class SelfInjectedBean
 */
@Stateless
public class SelfInjectedBean implements SelfInjectedBeanLocal {

	@Resource
	private SessionContext sessionContext;

	public String name() {
		SelfInjectedBeanLocal self = sessionContext.getBusinessObject(SelfInjectedBeanLocal.class);
		return "full name is :" + self.generateNewName();
	}

	public String generateNewName() {
		return new Random().nextInt(1000) + "";
	}
}
