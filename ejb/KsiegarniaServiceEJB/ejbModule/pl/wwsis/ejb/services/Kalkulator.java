package pl.wwsis.ejb.services;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Session Bean implementation class Kalkulator
 */
@Singleton
@LocalBean
@Path("/kalkulator")
public class Kalkulator {

	/**
	 * Default constructor.
	 */
	public Kalkulator() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void initMe() {
		
	}
	
	@PreDestroy
	public void wyczyscMe() {
		
	}
	
	@GET
	@Produces("text/plain")
	@Path("/pomoc")
	public String hello() {
		return "/dodaj/a/b  ==> dodaje a i b";
	}
	
	
	
	@GET
	@Produces("application/json")
	@Path("/dodaj/{a}/{b}")
	@Consumes("application/xml")
	public Double dodaj(@PathParam("a") Double a, @PathParam("b") Double b) {
		return a + b;
	}

}
