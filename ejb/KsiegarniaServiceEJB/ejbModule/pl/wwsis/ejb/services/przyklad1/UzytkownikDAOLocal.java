package pl.wwsis.ejb.services.przyklad1;

import javax.ejb.Local;

@Local
public interface UzytkownikDAOLocal {
	void zapisz(String imie, String nazwisko, String login, String email, String haslo);
}
