package pl.wwsis.ejb.services.przyklad1;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;

/**
 * Session Bean implementation class UzytkownikStatefullBean
 */
@Stateful
public class UzytkownikStatefullBean implements UzytkownikStatefullBeanLocal {
	
	@EJB
	private UzytkownikDAOLocal uzytkownikDAO;
	
	private String imie;
	private String nazwisko;
	private String login;
	private String email;
	private String haslo;
	
	private boolean akceptacjaRegulaminu = false;
	
	/**
	 * Wprowadzenie danych przez uzytkownika
	 * 
	 * @see pl.wwsis.ejb.services.przyklad1.UzytkownikStatefullBeanLocal#wprowadzDane(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void wprowadzDane(String imie, String nazwisko, String login, String email, String haslo) {
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.login = login;
		this.email = email;
		this.haslo = haslo;
	}
	
	/**
	 * Akceptacja przez uzytkownika regulaminu
	 * 
	 * @see pl.wwsis.ejb.services.przyklad1.UzytkownikStatefullBeanLocal#akceptacja()
	 */
	public void akceptacja() {
		this.akceptacjaRegulaminu = true;
	}
	
	/**
	 * Ostatni krok w procesie tworzenia nowego użytkownika
	 * @see pl.wwsis.ejb.services.przyklad1.UzytkownikStatefullBeanLocal#utworzUzytkownika()
	 */
	@Remove
	public void utworzUzytkownika() {
		if(akceptacjaRegulaminu) {
			uzytkownikDAO.zapisz(imie, nazwisko, login, email, haslo);
		} else {
			throw new RuntimeException("Brak akceptacji regulaminu");
		}
	}
	
	@Remove
	public void anuluj() {
		// anuluj rejestrację użytkowika
	}
	
	@PreDestroy
	public void destroy() {
		this.email = null;
		this.haslo = null;
		this.imie = null;
		this.login = null;
		this.nazwisko = null;
	}
}
