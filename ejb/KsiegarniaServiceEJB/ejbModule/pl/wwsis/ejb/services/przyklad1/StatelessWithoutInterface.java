package pl.wwsis.ejb.services.przyklad1;

import javax.ejb.Local;
import javax.ejb.Stateless;

@Stateless
@Local(StatelessWithoutInterfaceLocal.class)
public class StatelessWithoutInterface {

	public String sayHello(String name) {
		return "Witaj jesteś w bezstanowym beanie " + name;
	}

}
