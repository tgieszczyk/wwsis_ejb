package pl.wwsis.ejb.services.przyklad1;

import javax.ejb.Local;

@Local
public interface StatelessWithInterfaceLocal {
	String sayHello(String name);
}
