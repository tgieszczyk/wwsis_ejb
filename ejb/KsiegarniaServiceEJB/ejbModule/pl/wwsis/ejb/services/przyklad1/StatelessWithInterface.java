package pl.wwsis.ejb.services.przyklad1;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class StatelessWithInterface
 */
@Stateless
public class StatelessWithInterface implements StatelessWithInterfaceLocal {

	public String sayHello(String name) {
		return "Witaj " + name;
	};

}
