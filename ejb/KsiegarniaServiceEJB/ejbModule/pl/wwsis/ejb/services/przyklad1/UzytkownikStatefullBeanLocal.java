package pl.wwsis.ejb.services.przyklad1;

import javax.ejb.Local;

@Local()
public interface UzytkownikStatefullBeanLocal {
	void wprowadzDane(String imie, String nazwisko, String login, String email, String haslo);
	void akceptacja();
	void utworzUzytkownika();
	void anuluj();
}
