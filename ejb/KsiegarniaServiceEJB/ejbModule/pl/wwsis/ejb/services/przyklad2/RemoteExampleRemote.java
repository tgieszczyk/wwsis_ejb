package pl.wwsis.ejb.services.przyklad2;

import javax.ejb.Remote;

@Remote
public interface RemoteExampleRemote {
	String sayHello(String name);
}
