package pl.wwsis.ejb.services.przyklad2;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class RemoteExample
 */
@Stateless
public class RemoteExample implements RemoteExampleRemote {

	public String sayHello(String name) {
		// TODO Auto-generated method stub
		return String.format("Witaj '%s' !!!", name);
	}
}
