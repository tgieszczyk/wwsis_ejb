package pl.wwsis.ejb.services.exceptions;

import javax.ejb.ApplicationException;


/**
 * @author a051213
 *
 */
@ApplicationException(rollback=false)
public class BiznesowyWyjate extends Exception {

	public BiznesowyWyjate() {
		super();
	}

	public BiznesowyWyjate(String message, Throwable cause) {
		super(message, cause);
	}

	public BiznesowyWyjate(String message) {
		super(message);
	}

	public BiznesowyWyjate(Throwable cause) {
		super(cause);
	}

}
