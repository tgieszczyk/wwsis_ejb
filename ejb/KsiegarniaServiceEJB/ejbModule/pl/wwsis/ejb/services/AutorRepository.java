package pl.wwsis.ejb.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.wwsis.encje.Autor;

/**
 * Session Bean implementation class AutorRepository
 */
@Singleton
@LocalBean
public class AutorRepository {

	@Resource
	private SessionContext context;

	@PersistenceContext(unitName = "orm-unit")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public AutorRepository() {
		// TODO Auto-generated constructor stub
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void zapisz(String imie, String nazwisko) {
		Autor autor = new Autor(imie, nazwisko);
		zapisz(autor);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void zapisz(Autor autor) {
		em.persist(autor);

		context.getBusinessObject(AutorRepository.class).zapiszDoLogow(
				"Zapisalo sie ");
		zapiszDoLogow("zapisalow sie");
	}

	@SuppressWarnings("unchecked")
	public List<Autor> znajdzWszystkich() {
		List<Autor> psb = em.createQuery("SELECT a FROM " + Autor.class.getName() + " a JOIN FETCH a.ksiazki k JOIN FETCH k.wydawnictwo w JOIN FETCH w.adres")
				.getResultList();
		List<Autor> result = new ArrayList<>();

		for (Autor autor : psb) {
			result.add(autor);
		}
		return result;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void zapiszDoLogow(String message) {

	}
}
