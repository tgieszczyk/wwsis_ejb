package pl.wwsis.ejb;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class RemoteObjectLocator {

	private Context context;
	private String commonPrefix;
	public RemoteObjectLocator() throws NamingException {
		this(null);
	}

	public RemoteObjectLocator(String commonPrefix) throws NamingException {
		Properties properties = new Properties();
//		properties.put(Context.INITIAL_CONTEXT_FACTORY,
//				"org.apache.openejb.client.RemoteInitialContextFactory");
//		properties.put(Context.PROVIDER_URL,
//				"http://127.0.0.1:9080/przykladyWEB/myejbs");
//		this.context = new InitialContext(properties);
//		this.commonPrefix = commonPrefix;
//		
//		properties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
//		 
//        final Context context = new InitialContext(properties);
 
 
//        final String appName = "";
//        final String moduleName = "as7project";
//        final String distinctName = "";
//        final String beanName = SampleBeanRemoteImpl.class.getSimpleName();
// 
//        final String viewClassName = SampleBeanRemote.class.getName();
//        System.out.println("Looking EJB via JNDI ");
//        System.out.println("ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName);
 
//        return (SampleBeanRemote) context.lookup("ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName);
 
	}

	public void list() throws NamingException {
		NamingEnumeration<NameClassPair> list = context.list("");
		while (list.hasMore()) {
			NameClassPair pair = list.next();
			System.out.println(pair.getClassName());
		}
	}

	public Object lookup(String name) {
		try {
			if (commonPrefix != null)
				name = commonPrefix + "/" + name;
			return context.lookup(name);
		} catch (NamingException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
