package pl.wwsis.ejb;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import pl.wwsis.ejb.services.przyklad2.RemoteExampleRemote;

public class RemoteObjectLocatorTest {

	
	public static void main(String[] args) throws NamingException {
		
		Properties properties = new Properties();
		
		properties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
//		properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		properties.put(Context.PROVIDER_URL, "remoting://localhost:4447");
		properties.put(Context.SECURITY_PRINCIPAL, "tomasz");
		properties.put(Context.SECURITY_CREDENTIALS, "Tomasz11$");
		          
		properties.put("jboss.naming.client.ejb.context", true);
		
        final Context context = new InitialContext(properties);
 
        final String beanName = RemoteExampleRemote.class.getSimpleName();
 
        final String viewClassName = RemoteExampleRemote.class.getName();
 
        String jndiName = "ejb:app/JwdEjbWyklad1EAR/JwdEjbWyklad1EJB//RemoteExample!" + viewClassName;
        System.out.println("Looking EJB via JNDI ");
        System.out.println(jndiName);
        
        RemoteExampleRemote bean = (RemoteExampleRemote)context.lookup(jndiName);

        System.out.println(bean);
		System.out.println(bean.sayHello("Tomasz"));
	}

}
